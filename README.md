Interrupted sinusoidal map, each hemisphere split in nine lobes.

D3 v4.

Inspired by <a href="http://www.progonos.com/furuti/MapProj/Normal/ProjInt/projInt.html">Futuri's Interrupted Maps</a>.

forked from <a href='http://bl.ocks.org/mbostock/'>mbostock</a>'s block: <a href='http://bl.ocks.org/mbostock/4481265'>Interrupted Boggs Eumorphic</a>